using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float horizontal_speed = 10.0f;

    public float horizontal_limit = 1f;

    public Slider slider;

    private SpriteRenderer _renderer;

    public GameObject explosionPrefab;

    private GameObject explosionInstance;

    public GameObject gameoverText;

    void Start() {
        _renderer = GetComponent<SpriteRenderer>();
        InvokeRepeating("flipSprite", 0, 0.3f);

        explosionInstance = Instantiate(explosionPrefab);
        explosionInstance.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Velocity from control
        GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * horizontal_speed,
        0);

        // Horizontal limit
        if (transform.position.x > horizontal_limit) {
            transform.position = new Vector2(horizontal_limit, transform.position.y);
        }
        if (transform.position.x < -horizontal_limit) {
            transform.position = new Vector2(-horizontal_limit, transform.position.y);
        }

        slider.value = transform.position.x;
    }

    private void OnDestroy() {
        explosionInstance.transform.position = transform.position;
        explosionInstance.SetActive(true);
        gameoverText.SetActive(true);
    }

    private void flipSprite() {
        _renderer.flipX = !_renderer.flipX;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("White")) {
            Destroy(gameObject);
        } else if (other.gameObject.CompareTag("Red")) {
            if (_renderer.color[1] > 0) {
                _renderer.color = new Color(
                    _renderer.color[0]-0.0f,
                    _renderer.color[1]-0.05f,
                    _renderer.color[2]-0.05f);
            }
        }
    }
}
