using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public float explosionTime = 1.0f;
    private float explosionTimer;
    // Start is called before the first frame update
    void Start()
    {
        explosionTimer = explosionTime;
    }

    // Update is called once per frame
    void Update()
    {
        explosionTimer -= Time.deltaTime;
        if (explosionTimer <= 0.0f) {
            gameObject.SetActive(false);
            explosionTimer = explosionTime;
        }
    }
}
