using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellController : MonoBehaviour
{
    private SpriteRenderer _renderer;

    protected void Start() {
        _renderer = GetComponent<SpriteRenderer>();
        InvokeRepeating("flipSprite", 0, 0.8f);
    }

    protected void OnEnable() {
        var random_x = Random.Range(-1, 2);
        transform.position =  new Vector2(random_x, 5);
    }

    // Update is called once per frame
    protected void Update()
    {
        var speed = transform.parent.gameObject.GetComponent<GameController>().speed;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);

        if (transform.position.y < -5f) {
            gameObject.SetActive(false);
        }
    }

    private void flipSprite() {
        _renderer.flipX = !_renderer.flipX;
    }
}
