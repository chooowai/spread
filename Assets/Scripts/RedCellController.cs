using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedCellController : CellController
{
    public Sprite redSprite;
    public Sprite greenSprite;
    SpriteRenderer spriteRenderer;

    new void OnEnable() {
        if (spriteRenderer == null) {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
        spriteRenderer.sprite = redSprite;
        base.OnEnable();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Player")) {
            spriteRenderer.sprite = greenSprite;
            if (OnSpread != null) {
                OnSpread();
            }
        }
    }

    public delegate void SpreadHandler();
    public event SpreadHandler OnSpread;
}
