using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
    public float speed = 3f;
    public int energy = 10;
    public int score = 0;
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI scoreText;

    public ObjectPool redCellPool;
    public ObjectPool whiteCellPool;

    public float cellSpawnInterval = 0.8f;
    private float cellSpawnTimer = 0.0f;

    public GameObject player;

    AudioSource audioData;
    public AudioClip bgmClip;
    public AudioClip correctClip;
    public AudioClip incorrectClip;
    public AudioClip incorrectClip2;
    bool gameOn = true;

    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();
        startGame();
    }

    void startGame() {
        InvokeRepeating("consumeEnergy", 0, 1.0f);
        InvokeRepeating("increaseScore", 0, 1.0f);
        InvokeRepeating("increaseSpeed", 0, 5.0f);
        cellSpawnTimer = 0.0f;
        audioData.PlayOneShot(bgmClip, 0.4f);
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null) {
            if(Time.time >= cellSpawnTimer) {
                cellSpawnTimer = Time.time + cellSpawnInterval;
                var random_n = Random.Range(0, 2);
                if (random_n == 0) {
                    createRedCell();
                } else {
                    createWhiteCell();
                }
            }
        } else if (gameOn) {
            audioData.Stop();
            audioData.PlayOneShot(incorrectClip, 1f);
            audioData.PlayOneShot(incorrectClip2, 1f);
            gameOn = false;
        }
    }

    private void createRedCell() {
        GameObject redCellObject = redCellPool.getObject();

        redCellObject.GetComponent<RedCellController>().OnSpread -= OnVirusSpread;
        redCellObject.GetComponent<RedCellController>().OnSpread += OnVirusSpread;
    }

    private void createWhiteCell() {
        GameObject whiteCellObject = whiteCellPool.getObject();
    }

    private void consumeEnergy() {
        if (player != null) {
            energy -= 1;
            energyText.text = energy.ToString("0");
            if (energy <= 0) {
                Destroy(player);
            }
        }
    }

    private void increaseScore() {
        if (player != null) {
            score += 1;
            scoreText.text = score.ToString("0");
        }
    }

    private void increaseSpeed() {
        speed += 0.5f;
        if (cellSpawnInterval > 0) {
            cellSpawnInterval -= 0.08f;
        }
    }

    void OnVirusSpread() {
        energy += 1;
        energyText.text = energy.ToString("0");
        audioData.PlayOneShot(correctClip, 1f);
    }
}
