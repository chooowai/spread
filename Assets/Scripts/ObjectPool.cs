using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public GameObject poolPrefab;
    public int initialNum = 10;

    List<GameObject> poolObjects;

    public GameObject getObject() {
        for(int i=0;i<poolObjects.Count;i++) {
            if(!poolObjects[i].activeInHierarchy) {
                poolObjects[i].SetActive(true);
                return poolObjects[i];
            }
        }
        GameObject newObj =  CreateObj();
        newObj.SetActive(true);
        return newObj;
    }

    private void Awake() {
        if (poolObjects == null) {
            InitPool();
        }
    }

    public void InitPool() {
        poolObjects = new List<GameObject>();
        for(int i=0;i<initialNum;i++) {
            CreateObj();
        }
    }

    GameObject CreateObj() {
        GameObject newObj = Instantiate(poolPrefab);
        newObj.transform.SetParent(transform.parent);
        newObj.SetActive(false);
        poolObjects.Add(newObj);
        return newObj;
    }
}